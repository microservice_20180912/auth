INSERT INTO s_permission (id, permission_value, permission_label) VALUES
  ('configuresystem', 'CONFIGURE_SYSTEM', 'Configure System');

INSERT INTO s_role (id, description, name) VALUES
  ('superuser', 'SUPERUSER', 'Super User'),
  ('clientapp', 'CLIENTAPP', 'Client Application'),
  ('staff', 'STAFF', 'Staff'),
  ('manager', 'MANAGER', 'Manager');

INSERT INTO s_role_permission (id_role, id_permission) VALUES
  ('superuser', 'configuresystem');

INSERT INTO s_user (id, active, username, id_role) VALUES
  ('u001', true, 'user001', 'superuser');

INSERT INTO s_user_password (id_user, password) VALUES
  -- password : abcd
  ('u001', '$2a$10$SQdrjU5ePvnihz4BlgTf6.Tg5F1TbrWT3BDGrVuTDtT/q7HR92u0S');