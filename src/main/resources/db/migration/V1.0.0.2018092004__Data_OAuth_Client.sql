insert into oauth_client_details (client_id, resource_ids, client_secret, scope, authorized_grant_types, web_server_redirect_uri)
values ('client001', 'belajar', '$2a$10$rk7SKH0KPDnMWVmFPenbt.dWDgkFd5R03GKpCKzwfRr3fNTlNt03G', 'all', 'authorization_code,refresh_token', 'http://localhost:10000');

-- grant_types : authorization_code,refresh_token,password,client
